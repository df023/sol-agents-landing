import React from "react";

import {
  Hero,
  Benefits,
  Process,
  Usage,
  Signup,
  FAQ,
} from "components/sections";
import ScrollTopBtn from "components/ScrollTopBtn";

export default function App() {
  return (
    <>
      <main className="pb-34">
        <Hero className="mb-18 md:mb-64" />
        <div className="container space-y-34 md:space-y-84">
          <Benefits />
          <Process />
          <Usage />
          <Signup />
          <FAQ />
        </div>
      </main>
      <ScrollTopBtn />
    </>
  );
}
