import React, { useRef, useEffect } from "react";
import debounce from "lodash/debounce";

import { ReactComponent as ArrowIcon } from "assets/img/arrow.svg";

export default function ScrollTopBtn() {
  const button = useRef();

  useEffect(() => {
    let prevScrollY = 0;
    let isVisible = false;
    window.addEventListener(
      "scroll",
      debounce(() => {
        const scrollY = window.scrollY;

        if (scrollY > 600) {
          if (prevScrollY - scrollY > 99) {
            if (!isVisible) {
              button.current.classList.remove("is-hidden");
              isVisible = true;
            }
          } else {
            button.current.classList.add("is-hidden");
            isVisible = false;
          }
        } else if (scrollY < 300) {
          button.current.classList.add("is-hidden");
          isVisible = false;
        }

        prevScrollY = scrollY;
      }, 20),
      { passive: true }
    );
  });

  return (
    <button
      ref={button}
      className="scroll-button is-hidden"
      onClick={() => {
        window.scroll({ top: 0, behavior: "smooth" });
      }}
    >
      <ArrowIcon className="w-24 h-24 lg:w-48 lg:h-48" />
    </button>
  );
}
