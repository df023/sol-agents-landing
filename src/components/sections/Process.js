import React from "react";

export default function Process() {
  return (
    <section>
      <h2>The Process</h2>
      <p className="font-semibold text-center mb-28 lg:mb-34">
        You can qualify as SOL Agent, if you have an identity document and a
        proof of address in South Africa.
      </p>
      <div className="process flex lg:flex-col">
        <div className="process__diagram flex flex-col items-center text-24 font-semibold text-gray-400 mr-12 lg:flex-row lg:mx-120 lg:mb-34">
          <p className="process__diagram-item">1</p>
          <span className="process__arrow" role="presentation" />
          <p className="process__diagram-item">2</p>
          <span className="process__arrow" role="presentation" />
          <p className="process__diagram-item">3</p>
        </div>
        <ul className="process__list flex flex-col justify-between lg:flex-row lg:items-start">
          <li className="process__list-item">
            You download SOL app, verify your profile and proof of address
          </li>
          <li className="process__list-item">
            We register you as Agent, provide you Agent Portal access and
            materials
          </li>
          <li className="process__list-item">
            You can start registering customers
          </li>
        </ul>
      </div>
    </section>
  );
}
