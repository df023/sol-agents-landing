import React from "react";

import { ReactComponent as MoneyIcon } from "assets/img/money.svg";
import { ReactComponent as LightBulbIcon } from "assets/img/light-bulb.svg";
import { ReactComponent as BankIcon } from "assets/img/bank.svg";
import { ReactComponent as CartIcon } from "assets/img/cart.svg";

const usages = [
  {
    text:
      "Receive your salary<br>Or other payments - into your SOL app balance",
    Icon: MoneyIcon,
  },
  {
    text: "Purchase airtime and electricity<br>For you, friends and relatives",
    Icon: LightBulbIcon,
  },
  {
    text: "Pay to any bank<br>And any account in SA, by EFT from the SOL app",
    Icon: BankIcon,
  },
  {
    text: "Get SOL Card<br>And swipe it in shops or withdraw in ATMs",
    Icon: CartIcon,
  },
];

export default function Usage() {
  return (
    <section>
      <h2>SOL can be used to:</h2>
      <ul className="usage">
        {usages.map(({ text, Icon }, index) => (
          <li className="usage__card" key={index}>
            <div className="flex-shrink-0 flex justify-center items-center w-60 h-60 bg-yellow text-black-100 rounded-full mr-12 lg:w-116 lg:h-116 lg:mr-24">
              <Icon className="w-36 h-36 lg:w-68 lg:h-68" />
            </div>
            <p dangerouslySetInnerHTML={{ __html: text }} />
          </li>
        ))}
      </ul>
    </section>
  );
}
