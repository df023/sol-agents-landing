import React from "react";
import classnames from "classnames";

import Btn from "components/Btn";

import { ReactComponent as SolLogo } from "assets/img/sol-logo.svg";
import { ReactComponent as CircleLines } from "assets/img/circle-lines.svg";

import HeroBg from "assets/img/hero-bg.png";

import { ReactComponent as UsersIcon } from "assets/img/users.svg";
import { ReactComponent as UserCheckIcon } from "assets/img/user-check.svg";
import { ReactComponent as CalendarIcon } from "assets/img/calendar.svg";

export default function Hero({ className }) {
  return (
    <section
      className={classnames([
        "relative bg-green text-white-100 overflow-hidden",
        className,
      ])}
    >
      <div className="container min-h-screen flex flex-col lg:justify-between">
        <div className="mb-24 lg:mb-52">
          <a
            className="sol-logo"
            href="https://solwallet.co.za/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <SolLogo className="w-48 h-35 lg:w-82 lg:h-50" />
          </a>
        </div>
        <div className="flex flex-grow flex-col justify-between lg:flex-row lg:flex-grow-0">
          <div className="flex-grow flex flex-col justify-between lg:justify-start lg:max-w-609">
            <div className="flex flex-col lg:mb-34">
              <h1 className="text-18 font-bold mb-14 lg:text-72 lg:mb-34 lg:leading-104">
                Build your own business
              </h1>
              <p className="tracking-1 lg:leading-35">
                We drive financial inclusion by providing an easy way to pay and
                get paid to anyone in South Africa. Join our growing SOL Agents
                Team from the beginning, and build your own income stream.
              </p>
            </div>
            <div className="z-10 flex flex-col">
              <Btn
                className="mb-18 shadow lg:py-20 lg:mb-56 lg:max-w-372 lg:tracking-tighter"
                onClick={() => {
                  const signUpHeader = document.getElementById("sign-up");
                  signUpHeader.scrollIntoView({ behavior: "smooth" });
                  signUpHeader.focus();
                }}
              >
                Become an agent
              </Btn>
              <ul className="flex space-x-12 lg:space-x-24 lg:max-w-513 lg:mb-36">
                <li className="flex flex-col justify-between rounded-12 bg-white-200 text-black-200 p-8 w-1/3 shadow-md lg:p-16">
                  <p className="text-12 leading-16 font-semibold mb-4 lg:text-16 lg:font-bold lg:leading-22 lg:mb-8">
                    Registered customers
                  </p>
                  <div className="flex items-center lg:flex-col-reverse lg:items-start">
                    <div className="w-24 h-24 bg-yellow rounded-full mr-8 flex justify-center items-center lg:w-34 lg:h-34">
                      <UsersIcon className="w-20 h-20 lg:w-28 lg:h-28" />
                    </div>
                    <p className="font-bold lg:text-24 lg:tracking-tight lg:leading-33 lg:mb-4">9000</p>
                  </div>
                </li>
                <li className="flex flex-col justify-between rounded-12 bg-white-200 text-black-200 p-8 w-1/3 shadow-md lg:p-16">
                  <p className="text-12 leading-16 font-semibold mb-4 lg:text-16 lg:font-bold lg:leading-22 lg:mb-8">
                    Days since launch
                  </p>
                  <div className="flex items-center lg:flex-col-reverse lg:items-start">
                    <div className="w-24 h-24 bg-yellow rounded-full mr-8 flex justify-center items-center lg:w-34 lg:h-34">
                      <CalendarIcon className="w-14 h-14 lg:w-20 lg:h-20" />
                    </div>
                    <p className="font-bold lg:text-24 lg:tracking-tight lg:leading-33 lg:mb-4">60</p>
                  </div>
                </li>
                <li className="flex flex-col justify-between rounded-12 bg-white-200 text-black-200 p-8 w-1/3 shadow-md lg:p-16">
                  <p className="text-12 leading-16 font-semibold mb-4 lg:text-16 lg:font-bold lg:leading-22 lg:mb-8">
                    Agents across S
                    <span className="hidden lg:inline">outh </span>A
                    <span className="hidden lg:inline">frica</span>
                  </p>
                  <div className="flex items-center lg:flex-col-reverse lg:items-start">
                    <div className="w-24 h-24 bg-yellow rounded-full mr-8 flex justify-center items-center lg:w-34 lg:h-34">
                      <UserCheckIcon className="w-20 h-20 lg:w-28 lg:h-28" />
                    </div>
                    <p className="font-bold lg:text-24 lg:tracking-tight lg:leading-33 lg:mb-4">30</p>
                  </div>
                </li>
              </ul>
            </div>
          </div>

          <div className="hero-image">
            <div className="hero-image__container">
              <img
                className="hero-image__front"
                src={HeroBg}
                alt="Yong black person with glasses and phone"
              />
              <CircleLines className="hero-image__back" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
