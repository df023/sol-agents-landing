import React, { useState } from "react";

import { registerAgent } from "api";

import Btn from "components/Btn";
import InputField from "components/InputField";

import { ReactComponent as CheckIcon } from "assets/img/checkmark.svg";
import { ReactComponent as ErrorIcon } from "assets/img/error.svg";

import { required, charsOnly, email, phoneNumber } from "helpers/validation";

const validation = {
  name: [required, charsOnly],
  email: [required, email],
  phone: [required, phoneNumber],
};

function validate(field, value) {
  const validations = validation[field];
  for (let validation of validations) {
    const error = validation(field, value);
    if (error) {
      return error;
    }
  }

  return "";
}

export default function Signup() {
  const [isLoading, setIsLoading] = useState(false);
  const [isRegistered, setIsRegistered] = useState(false);
  const [registerError, setRegisterError] = useState(null);
  const [{ form, errors }, setFormState] = useState({
    form: {
      name: "",
      email: "",
      phone: "",
    },
    errors: {
      name: null,
      email: null,
      phone: null,
    },
  });

  async function handleSubmit(event) {
    event.preventDefault();

    let hasErrors = false;

    setFormState((prevState) => {
      const errors = {};

      for (let [field, value] of Object.entries(form)) {
        const error = validate(field, value);
        errors[field] = error;
        if (error) hasErrors = true;
      }

      return { ...prevState, errors };
    });

    if (hasErrors) return;

    try {
      setIsLoading(true);
      await registerAgent(form);
      setIsRegistered(true);
    } catch (error) {
      if (error.response) {
        setRegisterError(error.response);
      } else {
        setRegisterError("Sorry, there was an error on the server");
      }
    } finally {
      setIsLoading(false);
    }
  }

  function updateForm(field, value) {
    setFormState((prevState) => {
      return {
        form: { ...prevState.form, [field]: value },
        errors: { ...prevState.errors, [field]: validate(field, value) },
      };
    });
  }

  const hasValidationErrors = Object.values(errors).some((error) => error);

  let content;

  if (registerError) {
    content = (
      <div className="flex flex-col items-center">
        <span className="flex justify-center items-center bg-gray-500 rounded-full w-60 h-60 mb-14 lg:w-120 lg:h-120">
          <ErrorIcon className="w-30 h-24 lg:w-60 lg:h-46" />
        </span>
        <p className="mb-18 lg:mb-30">{registerError}</p>
        <Btn className="text-12 shadow" onClick={() => setRegisterError(null)}>
          Try again
        </Btn>
      </div>
    );
  } else if (isRegistered) {
    content = (
      <div className="flex flex-col items-center">
        <span className="flex justify-center items-center bg-gray-500 rounded-full w-60 h-60 mb-14 lg:w-120 lg:h-120">
          <CheckIcon className="w-30 h-24 lg:w-60 lg:h-46" />
        </span>
        <p>Success, you are registered!</p>
      </div>
    );
  } else {
    content = (
      <form className="w-full" noValidate onSubmit={handleSubmit}>
        <div className="flex flex-col max-w-420 m-auto space-y-34 lg:space-y-40 lg:mb-14">
          <InputField
            id="name"
            name="name"
            label="Name:"
            value={form.name}
            error={errors.name}
            onChange={updateForm}
          />
          <InputField
            type="email"
            id="email"
            name="email"
            label="Email:"
            value={form.email}
            error={errors.email}
            onChange={updateForm}
          />
          <InputField
            type="tel"
            id="phone"
            name="phone"
            label="Phone:"
            mask="+27 99 999 9999"
            value={form.phone}
            error={errors.phone}
            onChange={updateForm}
          />
          <Btn className="shadow" disabled={hasValidationErrors || isLoading}>
            Sign Up
          </Btn>
          {registerError && <span className="text-red">{registerError}</span>}
        </div>
      </form>
    );
  }

  return (
    <section>
      <h2 id="sign-up">Sign up and build your own income!</h2>
      <div className="flex justify-center items-center bg-white-200 p-12 rounded-12 shadow border-white-300 min-h-308 lg:max-w-1080 lg:m-auto lg:p-34 md:min-h-310 md:bg-white-100 md:shadow-none md:border lg:min-h-428">
        {content}
      </div>
    </section>
  );
}
