import React from "react";

import Btn from "components/Btn";

export default function BenefitsItem({ title, list, Icon }) {
  return (
    <div className="flex flex-col bg-white-200 px-12 py-16 rounded-12 shadow lg:w-1/3 lg:p-24 lg:text-18">
      <h3 className="flex justify-center items-center text-16 font-semibold leading-24 mb-24 lg:text-24 lg:mb-30">
        <div className="flex justify-center items-center w-24 h-24 bg-white-100 rounded-full mr-8 lg:w-36 lg:h-36">
          <Icon className="w-18 h-18 lg:w-26 lg:h-26" />
        </div>
        {title}
      </h3>
      <ul className="benefits mb-24 space-y-24 lg:space-y-30 lg:flex-grow lg:mb-44">
        {list.map((item, index) => (
          <li className="benefits__item" key={index}>
            {item}
          </li>
        ))}
      </ul>
      <Btn
        className="mb-8 shadow"
        onClick={() => {
          const signUpHeader = document.getElementById("sign-up");
          signUpHeader.scrollIntoView({ behavior: "smooth" });
          signUpHeader.focus();
        }}
      >
        Sign Up
      </Btn>
    </div>
  );
}
