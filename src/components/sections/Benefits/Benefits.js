import React from "react";

import BenefitsItem from "./BenefitsItem";

import { ReactComponent as MoneyCalcIcon } from "assets/img/money-calc.svg";
import { ReactComponent as SupportIcon } from "assets/img/support.svg";
import { ReactComponent as RocketIcon } from "assets/img/rocket.svg";

const benefitsList = [
  {
    title: "Stable income",
    titleIcon: MoneyCalcIcon,
    benefits: [
      "earn R60 from every activated card",
      "earn R20 from activations of your customers referrals",
      "drive your sales the way you like",
      "work within your own schedule",
    ],
  },
  {
    title: "Build your career",
    titleIcon: SupportIcon,
    benefits: [
      "build your own business and income streams",
      "become a Super Agent and hire your own team",
      "earn from the team sales",
      "become your own boss",
    ],
  },
  {
    title: "Easy start",
    titleIcon: RocketIcon,
    benefits: [
      "free sign up",
      "registration takes few minutes",
      "we provide the materials and trainings",
      "start earning on the next day",
    ],
  },
];

export default function Benefits() {
  return (
    <section>
      <h2>
        Benefits for SOL Agents
      </h2>
      <div className="flex flex-col space-y-14 lg:flex-row lg:space-y-0 lg:space-x-20">
        {benefitsList.map(({ title, titleIcon, benefits }, index) => (
          <BenefitsItem
            title={title}
            list={benefits}
            Icon={titleIcon}
            key={index}
          />
        ))}
      </div>
    </section>
  );
}
