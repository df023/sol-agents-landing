import Hero from "./Hero";
import Benefits from "./Benefits/Benefits";
import Process from "./Process";
import Usage from "./Usage";
import Signup from "./Signup";
import FAQ from "./FAQ";

export { Hero, Benefits, Process, Usage, Signup, FAQ };
