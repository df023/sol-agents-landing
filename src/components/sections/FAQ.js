import React, { useState } from "react";

import Accordion from "components/Accordion";

import questions from "static/faq.json";

export default function FAQ() {
  const [activeQuestion, setActiveQuestion] = useState(null);

  return (
    <section>
      <h2>Frequently asked questions</h2>
      <div className="max-w-1080 m-auto">
        {questions.map(({ question, answer }, index) => (
          <Accordion
            title={question}
            text={answer}
            isOpen={index === activeQuestion}
            handleClick={() =>
              setActiveQuestion(index === activeQuestion ? null : index)
            }
            key={index}
          />
        ))}
      </div>
    </section>
  );
}
