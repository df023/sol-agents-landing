import React from "react";
import classnames from "classnames";
import InputMask from "react-input-mask";

import { ReactComponent as PlusIcon } from "assets/img/plus.svg";

export default function InputField({
  className,
  id,
  label,
  placeholder,
  value,
  error,
  onChange,
  ...props
}) {
  function handleChange(e) {
    onChange(e.target.name, e.target.value);
  }

  function clearInput() {
    onChange(props.name, "");
    setTimeout(() => document.getElementById(id).focus());
  }

  const Input = props.mask ? InputMask : "input";

  return (
    <div
      className={classnames([
        {
          "input-field": true,
          "is-empty": value.length === 0,
          "has-error": !!error,
        },
        className,
      ])}
    >
      <label htmlFor={id} className="input-field__label">
        {label}
      </label>

      <Input
        id={id}
        className="input-field__input"
        {...props}
        value={value}
        onChange={handleChange}
      />
      <button
        type="button"
        className="input-field__clear-button"
        onMouseDown={clearInput}
      >
        <PlusIcon className="w-24 h-24" />
      </button>
      <span className="input-field__error-text">{error}</span>
    </div>
  );
}
