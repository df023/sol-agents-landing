import React from "react";
import classnames from "classnames";

export default function Btn({ className, children, disabled, ...attrs }) {
  return (
    <button
      className={classnames([
        "text-18 leading-22 px-16 py-11 rounded-12 font-semibold lg:py-17 lg:text-20",
        { "bg-gray-300 text-white-100": disabled },
        { "bg-yellow text-black-300": !disabled },
        className,
      ])}
      disabled={disabled}
      {...attrs}
    >
      {children}
    </button>
  );
}
