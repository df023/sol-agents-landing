import React, { useRef } from "react";
import classnames from "classnames";

import { ReactComponent as MinusIcon } from "assets/img/minus.svg";
import { ReactComponent as PlusIcon } from "assets/img/plus.svg";

export default function Accordion({ title, text, isOpen, handleClick }) {
  const content = useRef();
  const classes = ["accordion relative py-12 border-t border-b lg:text-18 lg:leading-22 lg:py-16 lg:px-24"];
  let Icon;
  let maxHeight = null;

  if (isOpen) {
    Icon = MinusIcon;
    classes.push("border-yellow z-10");
    maxHeight = `${content.current.scrollHeight}px`;
  } else {
    Icon = PlusIcon;
    classes.push("border-gray-700");
  }

  return (
    <div className={classnames(classes)}>
      <button
        className="w-full flex justify-between items-center font-semibold text-left"
        onClick={handleClick}
      >
        <span>{title}</span>
        <span className="w-40 h-40 flex justify-center items-center bg-yellow text-black-100 rounded-full ml-16">
          <Icon className="w-24 h-24" />
        </span>
      </button>
      <div ref={content} className="accordion__content lg:text-14" style={{ maxHeight }}>
        <p className="text-gray-300 mt-12">{text}</p>
      </div>
    </div>
  );
}
