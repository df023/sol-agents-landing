function normalizeData({ name, email, phone }) {
  return {
    name: name.replace(/\s+/g, " ").trim(),
    email: email.trim(),
    phone: phone.replace(/\s/g, ""),
  };
}

export async function registerAgent(formData) {
  const data = normalizeData(formData);
  const resp = await fetch(
    `${process.env.REACT_APP_BASE_URL}/agent/web-become-agent-register`,
    {
      method: "POST",
      headers: {
        "X-Client-Type": "android",
        "X-Client-Version": "181",
      },
      body: JSON.stringify(data),
    }
  );

  const respBody = await resp.json();

  if (!resp.ok) {
    const error = new Error();
    error.response = respBody.request_error.message;
    throw error;
  }

  return respBody;
}
